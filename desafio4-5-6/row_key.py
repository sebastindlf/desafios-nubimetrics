from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder.getOrCreate()

join_sales = spark.read.parquet("join_sales.parquet")
sales = join_sales.select(concat(col("date"),lit("-"),col("category_id")).alias("row_key"),"*")
sales.drop("category_id","date")\
    .show()