def generateMonthlyPathList(year, month, day):
    #Implementación de tu solución
    temp_list = []
    for d in range(1,int(day)+1):
        d = "0"+str(d) if d < 10 else d
        row = f"https://importantdata@location/{year}/{month}/{d}/"
        temp_list.append(row)
    return temp_list

if __name__ == "__main__":
    montly_path_list = generateMonthlyPathList("2021", "05", "17")
    for element in montly_path_list:
        print(element)