import datetime as dt
import requests as req
import json
import os
        
def create_folder(api_name = "search", file_format = "json"):
    date = dt.datetime.now()
    year = str(date.year)
    month = "0"+str(date.month) if date.month < 10 else date.month
    day = "0"+str(date.day) if date.day < 10 else date.day
    folder_name = api_name + file_format + year + month + day
    return folder_name

def main():
    category = "MLA1000"
    url = f"https://api.mercadolibre.com/sites/MLA/search?category={category}"
    response = req.get(url)
    if response.status_code == 200:
        content = response.json()

        folder_name = create_folder()
        file_name = f"listado{category}.json"
        path = f"{folder_name}/{file_name}"

        try:
            os.mkdir(folder_name)
        except:
            pass
        finally:
            with open(path, "w") as file:
                json.dump(content, file, indent=4)

if __name__ == "__main__":
    main()
