from pyspark.sql import SparkSession
from pyspark.sql.functions import *
spark = SparkSession.builder.getOrCreate()

# Obtener los dataframe
sales_df = spark.read.parquet("sales.parquet")
# sales_df.printSchema()
# sales_df.show()
currencies_df = spark.read.options(header=True, inferSchema=True, delimeter=',').csv("currenciesClean.csv")
# currencies_df.printSchema()
# currencies_df.show()

# Crear las vistas temporales:
sales_df.createTempView("SALES")
currencies_df.createTempView("CURRENCIES")
join_sales = spark.sql("select sal.*, cur.localToDolar \
        from CURRENCIES cur inner join SALES sal on cur.datetrack = sal.date \
        where cur.currencyID = sal.currency_id")
join_sales.printSchema()
join_sales.show()
join_sales.write.mode("overwrite").parquet("join_sales.parquet")

# Cantidad de items:
print(f"|currenciesClean:{currencies_df.count()}\t",f"|sales:{sales_df.count()}\t",f"|join_sales:{join_sales.count()}")

# Item mas vendido:
join_sales.createTempView("JOIN_SALES")
max_sale = join_sales.select(max("sales").alias("value"))\
    .createTempView("MAX_SALE_VALUE")
spark.sql("select sal.* from JOIN_SALES sal, MAX_SALE_VALUE mxv where sal.sales = mxv.value").show()

"""
- El campo que elegi para la conversion fue el de local_to_dolar, ya que me parecio el mas apropiado para el modelo debido a que el precio del articulo esta en moneda local.
    Tambien se podria usar (como en el ejemplo) el campo dolar_to_local, pero la conversion a dolar seria por medio de una division, en lugar de una multiplicacion directa, lo cual es un poco menos intuitivo desde mi punto de vista.
- Pasaria en este caso que la conversion deberia realizarse por el valor del dolar en el dia,por ejemplo seria un error facturar una compra actual con la tasa de conversion del dolar de hace un año.
    Lo mismo ocurriria en este caso, la compra debe ser facturada con su tasa de conversion correspondiente a la fecha de facturacion.
    A la hora de buscar la tasa de conversion se debe tener en cuenta la fecha de ambos modelos.
- En currenciesClean.csv hay 18 items, en sales.parquet hay 2060 items.
- El item con mas ventas fue el id:MPE438744458 "Pelotas X Mayor, Con Mallitas" con un total de 90 ventas.
- La tasa de cambio es correcta para el item id:MPE438744458 "Pelotas X Mayor, Con Mallitas" y para los items con currency_id = 'USD'.
"""