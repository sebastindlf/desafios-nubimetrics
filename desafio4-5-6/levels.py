from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import ArrayType, StringType, IntegerType

spark = SparkSession.builder.getOrCreate()
sales_df = spark.read.parquet("join_sales.parquet")

# level1_df = sales_df.groupBy("level1").agg(sum("sales").alias("sales")).agg(sum("sales"))
# level1_df = level1_df.withColumn("level",lit("level1"))
# level1_df.show()

# level2_df = sales_df.groupBy("level2").agg(sum("sales").alias("sales")).agg(sum("sales"))
# level2_df = level2_df.withColumn("level",lit("level2"))
# level2_df.show()

# level3_df = sales_df.groupBy("level3").agg(sum("sales").alias("sales")).agg(sum("sales"))
# level3_df = level3_df.withColumn("level",lit("level3"))
# level3_df.show()

# level4_df = sales_df.groupBy("level4").agg(sum("sales").alias("sales")).agg(sum("sales"))
# level4_df = level4_df.withColumn("level",lit("level4"))
# level4_df.show()

# level5_df = sales_df.groupBy("level5").agg(sum("sales").alias("sales")).agg(sum("sales"))
# level5_df = level5_df.withColumn("level",lit("level5"))
# level5_df.show()

# level6_df = sales_df.groupBy("level6").agg(sum("sales").alias("sales")).agg(sum("sales"))
# level6_df = level6_df.withColumn("level",lit("level6"))
# level6_df.show()

level7_df = sales_df.groupBy("level7").agg(sum("sales").alias("sales"))
level7_df = level7_df.withColumn("level",lit("level7"))
level7_df.show()


# @udf(returnType=ArrayType(StringType()))
# def get_levels_list(*levels):
#     array = []
#     for level in levels:
#         if level is not None:
#             array.append(level)
#     print(array)
#     return array

# sales_df = spark.read.parquet("join_sales.parquet").show()
# temp_level_df = sales_df.select(get_levels_list("level1","level2","level3","level4","level5","level6","level7").alias("levels"))
# temp_level_df.show()