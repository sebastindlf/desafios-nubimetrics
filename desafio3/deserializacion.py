import json
import os

def main():
    path="desafio 3/currenciesRaw"
    with open(path) as file:
        raw_rows = file.readlines()

    rows = []
    for row in raw_rows:
        rows.append(row[:-1].split("\t"))

    for columns in rows:
        for column in columns:
            if column == "0":
                columns.remove(column)
            if column.count(":")>=3:
                #columns[columns.index(column)] = column.split(" ")[0]
                columns.remove(column)
            if columns.count(column)>1:
                columns.remove(column)
    
    for columns in rows:
        for column in columns[3:]:
            try:
                columns[columns.index(column)] = str(format(float(column),".2f"))
            except:
                pass


    for columns in rows:
        print(columns)
    with open("desafio 3/currencies.csv", "w") as file:
        file.write("datetrack,country,currency,site,dolarToLocal,localToDolar\n")
        for columns in rows:
            file.write(f"{columns[2]},{columns[0]},{columns[1]},{columns[5]},{columns[3]},{columns[4]}\n" )

    
if __name__ == "__main__":
    main()
