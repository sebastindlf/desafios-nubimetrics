from path1 import generateMonthlyPathList

def generateLastDaysPaths(date, days):
    #Implementación de tu solución
    temp_list = generateMonthlyPathList(year=date[:4],month=date[4:6],day=date[6:])
    amount = len(temp_list) - days
    return temp_list[amount:]
    
if __name__ == "__main__":
    monthly_lastD_path_list = generateLastDaysPaths("20210410", 10)
    for e in monthly_lastD_path_list:
        print(e)
    pass